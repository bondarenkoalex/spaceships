//
//  AppDelegate.mm
//  Spaceships
//
//  Created by DeadDog on 01.09.13.
//  Copyright DeadDog 2013. All rights reserved.
//

#import "cocos2d.h"

#import "CCBReader.h"
#import "SHAppDelegate.h"

@interface SHAppDelegate()<SHRootNavigationControllerDelegate>

@end

@implementation SHAppDelegate

void uncaughtExceptionHandler(NSException *exception) {
    NSLog(@"uncaughtExceptionHnadler -- Exception %@", [exception description]);
    NSLog(@"Stack trace: %@", [exception callStackSymbols]);
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

	self.navigationController = [[SHRootNavigationController alloc] init];
    self.navigationController.reshapeDelegate = self;
	self.navigationController.navigationBarHidden = YES;
    
	[self.window setRootViewController:self.navigationController];
	[self.window makeKeyAndVisible];
    
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
	
	return YES;
}

-(void) applicationWillResignActive:(UIApplication *)application
{
	if( [self.navigationController visibleViewController] == self.navigationController.director)
		[[CCDirector sharedDirector] pause];
}

-(void) applicationDidBecomeActive:(UIApplication *)application
{
	[[CCDirector sharedDirector] setNextDeltaTimeZero:YES];
	if( [self.navigationController visibleViewController] == self.navigationController.director)
		[[CCDirector sharedDirector] resume];
}

-(void) applicationDidEnterBackground:(UIApplication*)application
{
	if( [self.navigationController visibleViewController] == self.navigationController.director)
		[[CCDirector sharedDirector] stopAnimation];
}

-(void) applicationWillEnterForeground:(UIApplication*)application
{
	if( [self.navigationController visibleViewController] == self.navigationController.director)
		[[CCDirector sharedDirector] startAnimation];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
	CC_DIRECTOR_END();
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
	[[CCDirector sharedDirector] purgeCachedData];
}

-(void) applicationSignificantTimeChange:(UIApplication *)application
{
	[[CCDirector sharedDirector] setNextDeltaTimeZero:YES];
}

#pragma mark - SHRootNavigationControllerDelegate

- (void)didReshapeProjectionWithNavigationController:(SHRootNavigationController *)navigationController
{
    [navigationController.director runWithScene:[CCBReader sceneWithNodeGraphFromFile:@"cocosbuilder/IntroLayer.ccbi"]];
}

@end

