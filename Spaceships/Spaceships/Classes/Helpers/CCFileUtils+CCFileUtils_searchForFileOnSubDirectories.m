//
//  CCFileUtils+CCFileUtils_searchForFileOnSubDirectories.m
//  Spaceships
//
//  Created by Bondarenko Alexander on 9/20/13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import "CCFileUtils+CCFileUtils_searchForFileOnSubDirectories.h"
#import <objc/runtime.h>

@implementation CCFileUtils (CCFileUtils_searchForFileOnSubDirectories)

- (NSString*)myfullPathFromRelativePath:(NSString*)relPath resolutionType:(ccResolutionType*)resolutionType
{
    NSString *path = [self myfullPathFromRelativePath:relPath resolutionType:resolutionType];
    
    NSString *firstPathVriant = [NSString stringWithFormat:@"cocosbuilder/%@",relPath];
    NSString *secondPathVriant = [NSString stringWithFormat:@"cocosbuilder/ccbResources/%@",relPath];
    
    if ([path isEqualToString:relPath])
    {
        path = [self myfullPathFromRelativePath:firstPathVriant resolutionType:resolutionType];
    }
    
    if ([path isEqualToString:firstPathVriant])
    {
        path = [self myfullPathFromRelativePath:secondPathVriant resolutionType:resolutionType];
    }
    
    if ([path isEqualToString:secondPathVriant])
    {
        path = [self myfullPathFromRelativePath:secondPathVriant resolutionType:resolutionType];
    }
    
    return path;
}

+ (void)load
{
    Method original, swizzle;
    
    original = class_getInstanceMethod(self, @selector(myfullPathFromRelativePath: resolutionType:));
    swizzle = class_getInstanceMethod(self, @selector(fullPathFromRelativePath: resolutionType:));
    
    method_exchangeImplementations(original, swizzle);
}

@end
