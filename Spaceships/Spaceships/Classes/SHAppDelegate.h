//
//  AppDelegate.h
//  Spaceships
//
//  Created by DeadDog on 01.09.13.
//  Copyright DeadDog 2013. All rights reserved.
//

#import <UIKit/UIKit.h>
#include "SHRootNavigationController.h"
#import "cocos2d.h"

@interface SHAppDelegate : NSObject <UIApplicationDelegate>

@property (nonatomic, strong) UIWindow *window;
@property (nonatomic, strong) SHRootNavigationController *navigationController;

@end
