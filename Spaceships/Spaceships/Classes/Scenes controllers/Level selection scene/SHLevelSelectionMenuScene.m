//
//  SHLevelSelectionMenuScene.m
//  Spaceships
//
//  Created by Bondarenko Alexander on 9/20/13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import "SHLevelSelectionMenuScene.h"
#import "cocos2d.h"
#import "CCBReader.h"
#import "SHShipSelectionScene.h"

@implementation SHLevelSelectionMenuScene

- (id)init
{
    self = [super init];
    if (self)
    {
        self.levelSelectionMenuLayer = (SHLevelSelectionMenuLayer *)[CCBReader nodeGraphFromFile:@"cocosbuilder/SHLevelSelectionMenuLayer.ccbi" owner:self];
        [self addChild:self.levelSelectionMenuLayer];
    }
    return self;
}

#pragma mark - Actions

- (void)protectionOfBuildingsButtonClick:(id)singleplayerButton
{
    [self runShipSelectionSceneWithLevelType:SHLevelTypeTowerDefence];
}

- (void)oneAgainstAllButtonClick:(id)multiplayerButton
{
    [self runShipSelectionSceneWithLevelType:SHLevelTypeDeathmatch];
}

- (void)backButtonClick:(id)backButton
{
    [[CCDirector sharedDirector] popScene];
}

#pragma mark - Private methods

- (void)runShipSelectionSceneWithLevelType:(SHLevelType)levelType
{
    self.levelData.levelType = levelType;
    
    SHShipSelectionScene *scene = [[SHShipSelectionScene alloc] init];
    
    scene.levelData = self.levelData;
    
    [[CCDirector sharedDirector] pushScene:scene];
}


@end
