//
//  SHLevelSelectionMenuScene.h
//  Spaceships
//
//  Created by Bondarenko Alexander on 9/20/13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import "CCScene.h"
#import "SHLevelSelectionMenuLayer.h"
#import "SHLevelData.h"

@interface SHLevelSelectionMenuScene : CCScene

@property (nonatomic, strong) SHLevelSelectionMenuLayer *levelSelectionMenuLayer;
@property (nonatomic, strong) SHLevelData *levelData;

- (void)protectionOfBuildingsButtonClick:(id)singleplayerButton;
- (void)oneAgainstAllButtonClick:(id)multiplayerButton;
- (void)backButtonClick:(id)backButton;

@end
