//
//  SHDirectorIOS.h
//  Spaceships
//
//  Created by DeadDog on 03.09.13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import "cocos2d.h"

@interface SHDirectorIOS : CCDirectorIOS

+ (CCDirectorIOS *)directorIOS;

@end
