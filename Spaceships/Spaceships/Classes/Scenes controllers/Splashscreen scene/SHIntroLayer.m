//
//  SHIntroLayer.m
//  Spaceships
//
//  Created by Petr Bilinskiy on 9/14/13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import "SHIntroLayer.h"
#import "SHMainMenuScene.h"

@implementation SHIntroLayer

- (void)onEnter
{
    [super onEnter];
    [self performSelector:@selector(runMainMenuScene) withObject:nil afterDelay:2.0];
}

- (void)runMainMenuScene
{
    [[CCDirector sharedDirector] replaceScene:[[SHMainMenuScene alloc] init]];
}

@end
