//
//  SHRootNavigationController.h
//  Spaceships
//
//  Created by DeadDog on 02.09.13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cocos2d.h"

@class SHRootNavigationController;

@protocol SHRootNavigationControllerDelegate <NSObject>

- (void)didReshapeProjectionWithNavigationController:(SHRootNavigationController *)navigationController;

@end

@interface SHRootNavigationController : UINavigationController<CCDirectorDelegate>

@property (nonatomic, weak) id<SHRootNavigationControllerDelegate>reshapeDelegate;
@property (nonatomic, weak) CCDirectorIOS *director;

@end
