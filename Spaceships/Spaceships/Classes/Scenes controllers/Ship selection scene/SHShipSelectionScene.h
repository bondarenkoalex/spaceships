//
//  SHShipSelectionScene.h
//  Spaceships
//
//  Created by Bondarenko Alexander on 9/20/13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import "CCScene.h"
#import "SHLevelData.h"
#import "SHShipSelectionLayer.h"

@interface SHShipSelectionScene : CCScene

@property (nonatomic, strong) SHLevelData *levelData;
@property (nonatomic, strong) SHShipSelectionLayer *shipSelectionLayer;

@end
