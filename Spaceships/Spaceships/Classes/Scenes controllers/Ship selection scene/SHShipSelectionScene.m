//
//  SHShipSelectionScene.m
//  Spaceships
//
//  Created by Bondarenko Alexander on 9/20/13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import "SHShipSelectionScene.h"
#import "cocos2d.h"
#import "CCBReader.h"

@implementation SHShipSelectionScene

- (id)init
{
    self = [super init];
    
    if (self)
    {
        self.shipSelectionLayer = (SHShipSelectionLayer *)[CCBReader nodeGraphFromFile:@"cocosbuilder/SHShipSelectionLayer.ccbi" owner:self];
        [self addChild:self.shipSelectionLayer];
    }
    
    return self;
}

#pragma mark - Actions

- (void)backButtonClick:(id)backButton
{
    [[CCDirector sharedDirector] popScene];
}


@end
