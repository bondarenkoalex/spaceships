//
//  SHShipSelectionLayer.h
//  Spaceships
//
//  Created by Bondarenko Alexander on 9/20/13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import "CCLayer.h"
#import "CCControlExtension.h"

@interface SHShipSelectionLayer : CCLayer

@property (nonatomic, strong) CCControlButton *backButton;

@end
