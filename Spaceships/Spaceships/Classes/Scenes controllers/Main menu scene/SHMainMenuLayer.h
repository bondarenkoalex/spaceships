//
//  SHMainMenuLayer.h
//  Spaceships
//
//  Created by Bondarenko Alexander on 9/20/13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import "CCLayer.h"
#import "CCControlExtension.h"

@interface SHMainMenuLayer : CCLayer

@property (nonatomic, strong) CCControlButton *singleplayerButton;
@property (nonatomic, strong) CCControlButton *multiplayerButton;
@property (nonatomic, strong) CCControlButton *shopButton;
@property (nonatomic, strong) CCControlButton *settingsButton;
@property (nonatomic, strong) CCControlButton *extraButton;

@end
