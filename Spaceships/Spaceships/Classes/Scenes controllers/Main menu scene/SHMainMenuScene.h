//
//  SHMainMenuScene.h
//  Spaceships
//
//  Created by Bondarenko Alexander on 9/20/13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import "CCScene.h"
#import "SHMainMenuLayer.h"

@interface SHMainMenuScene : CCScene

@property (nonatomic, strong) SHMainMenuLayer *menuLayer;

- (void)singleplayerButtonClick:(id)singleplayerButton;
- (void)multiplayerButtonClick:(id)multiplayerButton;
- (void)shopButtonClick:(id)shopButton;
- (void)settingsButtonClick:(id)settingsButton;
- (void)extraButtonClick:(id)extraButton;

@end
