//
//  SHMainMenuScene.m
//  Spaceships
//
//  Created by Bondarenko Alexander on 9/20/13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import "SHMainMenuScene.h"
#import "cocos2d.h"
#import "CCBReader.h"
#import "SHLevelSelectionMenuScene.h"
#import "SHLevelData.h"

@implementation SHMainMenuScene

- (id)init
{
    self = [super init];
    if (self)
    {
        self.menuLayer = (SHMainMenuLayer *)[CCBReader nodeGraphFromFile:@"cocosbuilder/SHMainMenuLayer.ccbi" owner:self];
        [self addChild:self.menuLayer];
    }
    return self;
}

#pragma mark - Actions

- (void)singleplayerButtonClick:(id)singleplayerButton
{
    [self runLevelSelectionSceneWithGameType:SHGameTypeSingleplayer];
}

- (void)multiplayerButtonClick:(id)multiplayerButton
{
    [self runLevelSelectionSceneWithGameType:SHGameTypeMultiplayer];
}

- (void)shopButtonClick:(id)shopButton
{
    
}

- (void)settingsButtonClick:(id)settingsButton
{
    
}

- (void)extraButtonClick:(id)extraButton
{
    
}

#pragma mark - Private methods

- (void)runLevelSelectionSceneWithGameType:(SHGameType)gameType
{
    SHLevelData *levelData = [[SHLevelData alloc] init];
    
    levelData.gameType = gameType;
    
    SHLevelSelectionMenuScene *scene = [[SHLevelSelectionMenuScene alloc] init];
    
    scene.levelData = levelData;
    
    [[CCDirector sharedDirector] pushScene:scene];
}


@end
