//
//  SHRootNavigationController.m
//  Spaceships
//
//  Created by DeadDog on 02.09.13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import "SHRootNavigationController.h"
#import "SHDirectorIOS.h"

@interface SHRootNavigationController ()

@property (nonatomic, readwrite) BOOL didReshape;

@end

@implementation SHRootNavigationController

- (id)init
{
    self = [super initWithRootViewController:[SHDirectorIOS directorIOS]];
    if (self)
    {
        self.didReshape = NO;
        [self.director setDelegate:self];
    }
    return self;
}

- (CCDirectorIOS *)director
{
    return [SHDirectorIOS directorIOS];
}

-(NSUInteger)supportedInterfaceOrientations
{
	if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
		return UIInterfaceOrientationMaskLandscape;
    }
	
	return UIInterfaceOrientationMaskLandscape;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
		return UIInterfaceOrientationIsLandscape(interfaceOrientation);
    }
	
	return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

-(void) directorDidReshapeProjection:(CCDirector*)director
{
    [self.reshapeDelegate didReshapeProjectionWithNavigationController:self];
}

@end
