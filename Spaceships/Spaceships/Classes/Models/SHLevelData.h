//
//  SHLevelData.h
//  Spaceships
//
//  Created by Bondarenko Alexander on 9/20/13.
//  Copyright (c) 2013 DeadDog. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    SHLevelTypeTowerDefence = 0,
    SHLevelTypeDeathmatch
}SHLevelType;

typedef enum {
    SHGameTypeSingleplayer = 0,
    SHGameTypeMultiplayer
}SHGameType;

@interface SHLevelData : NSObject

@property (nonatomic, readwrite) SHLevelType levelType;
@property (nonatomic, readwrite) SHGameType gameType;

@end
