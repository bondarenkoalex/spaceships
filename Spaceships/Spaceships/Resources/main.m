//
//  main.m
//  Spaceships
//
//  Created by DeadDog on 01.09.13.
//  Copyright DeadDog 2013. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[])
{
    @autoreleasepool 
    {
        int retVal = UIApplicationMain(argc, argv, nil, @"SHAppDelegate");
        return retVal;
    }
}
