//
//  main.m
//  Cocos2dTest
//
//  Created by Bondarenko Alexander on 10/1/13.
//  Copyright Sigma 2013. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    @autoreleasepool {
        int retVal = UIApplicationMain(argc, argv, nil, @"AppController");
        return retVal;
    }
}
