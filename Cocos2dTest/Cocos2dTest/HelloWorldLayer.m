//
//  HelloWorldLayer.m
//  Cocos2dTest
//
//  Created by Bondarenko Alexander on 10/1/13.
//  Copyright Sigma 2013. All rights reserved.
//

#import "HelloWorldLayer.h"
#import "AppDelegate.h"

@interface HelloWorldLayer ()

@property (nonatomic, strong) CCSprite *sprite;
@property (nonatomic, strong) CCAction *walkAction;
@property (nonatomic, strong) CCAction *moveAction;

@end

@implementation HelloWorldLayer

+(CCScene *) scene
{
	CCScene *scene = [CCScene node];
	
	HelloWorldLayer *layer = [HelloWorldLayer node];
	[scene addChild: layer];
    
	return scene;//dadsad
}

-(id) init
{
	if( (self=[super init]) ) {
        
        self.touchEnabled = YES;
	}
	return self;
}

- (void)onEnter
{
    [super onEnter];
    
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"AnimBear.plist"];
    
    CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"AnimBear.png"];
    
    [self addChild:spriteSheet];
    
    NSMutableArray *walkAnimFrames = [NSMutableArray array];
    for (int i=1; i<=8; i++) {
        [walkAnimFrames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"bear%d.png",i]]];
    }
    
    CCAnimation *walkAnim = [CCAnimation animationWithSpriteFrames:walkAnimFrames delay:0.1f];
    
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    self.sprite = [CCSprite spriteWithSpriteFrameName:@"bear1.png"];
    self.sprite.position = ccp(winSize.width, winSize.height/2);
    
    self.walkAction = [CCRepeatForever actionWithAction:
                       [CCAnimate actionWithAnimation:walkAnim]];
    
    [self.sprite runAction:self.walkAction];
    
    
    [spriteSheet addChild:self.sprite];
    
    CCFiniteTimeAction *action1 = [CCMoveTo actionWithDuration:3.0 position:ccp(0, winSize.height/2)];
    CCFiniteTimeAction *action2 = [CCCallFunc actionWithTarget:self selector:@selector(rotate)];
    CCFiniteTimeAction *action3 = [CCMoveTo actionWithDuration:3.0 position:ccp(winSize.width, winSize.height/2)];
    
    [self.sprite runAction:[CCRepeatForever actionWithAction:[CCSequence actions:action1,action2,action3,action2,  nil]]];
}

- (void)rotate
{
    if (self.sprite.rotationY  == 180){
        self.sprite.rotationY = 0;
    }
    else{
        self.sprite.rotationY = 180;
    }
}

@end
