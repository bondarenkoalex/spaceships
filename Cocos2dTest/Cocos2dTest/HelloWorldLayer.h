//
//  HelloWorldLayer.h
//  Cocos2dTest
//
//  Created by Bondarenko Alexander on 10/1/13.
//  Copyright Sigma 2013. All rights reserved.
//


#import <GameKit/GameKit.h>
#import "cocos2d.h"

@interface HelloWorldLayer : CCLayer

+(CCScene *) scene;

@end
