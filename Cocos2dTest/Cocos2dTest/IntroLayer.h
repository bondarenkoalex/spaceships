//
//  IntroLayer.h
//  Cocos2dTest
//
//  Created by Bondarenko Alexander on 10/1/13.
//  Copyright Sigma 2013. All rights reserved.
//


// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"

// HelloWorldLayer
@interface IntroLayer : CCLayer
{
}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;

@end
